How to build your project:
1) Login to bitbucket.
2) In the navigation bar, click on create project.
3) Fill out the form.
4) Fill out the project name and key. It will be used as an identifier for your project and will appear in the URLs.
5) Optionally, you can choose an avatar for the project. This is displayed throughout Bitbucket and helps to identify your project.
6) Click Create project when you're done.

How to use an existing project:
To import code using the web interface:- 
1) While viewing a project within Bitbucket click Import repository in the sidebar.
2) Select a source to import code from, provide the required information, then click Connect.
a) For Bitbucket Cloud, include the Username and App password for the account to import from, and ensure read access for account, team, project, and repository is enabled.
b) For GitHub, include the Username and Personal access token for the account to import from, and ensure the repo and read:org scopes are enabled.
c) For GitHub Enterprise, provide the Server URL, Username, and Personal access token for the account to import from, and ensure the repo and read:org scopes are enabled.
d) For a single Git repository, provide the Clone URL, and Username and Password (if required).
3) Choose which repositories to import.
a) All repositories imports all the repositories owned by the account provided.
b) Select repositories allows you choose specific repositories to import.
4) Click Import.
Once importing completes, you can refresh the project page to see the imported repositories.


Note: The Academic free license allows anyone to enhance the project and use the source code.